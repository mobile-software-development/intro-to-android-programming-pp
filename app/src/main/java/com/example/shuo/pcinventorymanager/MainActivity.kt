package com.example.shuo.pcinventorymanager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        selectedColor = when (radioGroup.checkedRadioButtonId) {
            black_radioButton.id -> "Black"
            white_radioButton.id -> "White"
            else -> "Black"
        }
    }

    class Computer {
        var name = ""
        var color = "Black"
    }

    var selectedColor = "Black"
    val pcList = ArrayList<Computer>()
    var showBlack = true
    var showWhite = true
    var sortName = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        black_radioButton.setOnClickListener(this)
        white_radioButton.setOnClickListener(this)
        add_button.setOnClickListener {
            val computer = Computer()
            computer.name = name_text.text.toString()
            computer.color = selectedColor
            pcList.add(computer)
            showComputer()
        }
        black_checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            showBlack = isChecked
            showComputer()
        }
        white_checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            showWhite = isChecked
            showComputer()
        }
        sortname_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            sortName = isChecked
            showComputer()
        }
    }

    fun showComputer() {
        var tempList =
            pcList.filter { showBlack && it.color == "Black" || showWhite && it.color == "White" }
        if (sortName) {
            tempList = tempList.sortedBy { it.name }
        }
        var info = ""
        for (c in tempList) {
            info += "name: ${c.name}, color: ${c.color}\n"
        }
        info_text.text = info
    }
}
